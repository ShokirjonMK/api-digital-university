<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%student_mark}}`.
 */
class m221227_044617_create_student_mark_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableName = Yii::$app->db->tablePrefix . 'student_mark';
        if (!(Yii::$app->db->getTableSchema($tableName, true) === null)) {
            $this->dropTable('student_mark');
        }

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // https://stackoverflow.com/questions/51278467/mysql-collation-utf8mb4-unicode-ci-vs-utf8mb4-default-collation
            // https://www.eversql.com/mysql-utf8-vs-utf8mb4-whats-the-difference-between-utf8-and-utf8mb4/
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%student_mark}}', [
            'id' => $this->primaryKey(),

            'student_id' => $this->integer(11)->notNull(),
            'subject_id' => $this->integer(11)->notNull(),
            'course_id' => $this->integer(11)->null(),
            'semestr_id' => $this->integer(11)->null(),

            'edu_year_id' => $this->integer(11)->null(),

            'description' => $this->text()->null(),

            'status' => $this->tinyInteger(1)->defaultValue(1),
            'is_deleted' => $this->tinyInteger(1)->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'created_by' => $this->integer()->notNull()->defaultValue(0),
            'updated_by' => $this->integer()->notNull()->defaultValue(0),

        ], $tableOptions);

        $this->addForeignKey('mark_student_mark_student_id', 'student_mark', 'student_id', 'student', 'id');
        $this->addForeignKey('mark_student_mark_subject_id', 'student_mark', 'subject_id', 'subject', 'id');
        $this->addForeignKey('mark_student_mark_edu_year_id', 'student_mark', 'edu_year_id', 'edu_year', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('mark_student_mark_student_id', 'student_order');
        $this->dropForeignKey('mark_student_mark_subject_id', 'student_order');
        $this->dropForeignKey('mark_student_mark_edu_year_id', 'student_order');

        $this->dropTable('{{%student_mark}}');
    }
}
